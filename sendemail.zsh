#!/bin/zsh

if [[ $# -ne 3 ]]; then
  if [ -z "$1" ]; then
    vared -p 'To: ' -c target;
  else target = $1;
  fi
  if [ -z "$2" ]; then
    vared -p 'Subj: ' -c subject;
  else subject = $2;
  fi
  if [ -z "$3" ]; then
    vared -p 'Msg: ' -c message;
  else message = $3;
  fi
  echo "Is this correct?";
  echo "To: $target";
  echo "Sub: $subject";
  echo "Msg: $message";
  add=("${(s/:/)"$(cat ~/case)"}");
  select yn in $add; do
    case $yn in
      "$add[1]" ) echo "sending"; break;;
      "$add[2]" ) ~scr/sendemail.zsh; wait; kill $$; break;;
      "$add[3]" ) kill $$;  break;;
    esac
  done
# else
#   if [ -z "$1" ]; then
#     echo "no target";
#     kill $$;
#   fi
#   if [ -z "$2" ]; then
#     echo "no subject";
#     kill $$;
#   fi
#   if [ -z "$3" ]; then
#     echo "no message";
#     kill $$;
#   fi
fi
if [[ ! $target = ?*@?*.?* ]]; then
  echo "$target doesn't appear to be a valid email"; kill $$;
fi
sendemail -f nathan.yonkee@gmail.com -t $target -u $subject -m $message -s smtp.gmail.com:587 -o tls=yes -xu nathan.yonkee@gmail.com -xp Bandit01!