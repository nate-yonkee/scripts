#!/bin/zsh

while true
do
  dmenu -fn "-xos4-terminus-medium-r-*-*-24-*" -l 40 < ~/.xmonad/menufifo  | cut -c $1- | xdotool -
done
