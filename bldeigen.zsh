#!/bin/zsh

cd ~bld/eigen
echo "Do you wish to rm $PWD? (enter #)"
select yn in "Yes" "No"; do
  case $yn in
    Yes ) rm -r $PWD/*; break;;
    No ) kill $$; break;;
  esac
done
echo "Where to install to? (enter #)"
select yn in "$PWD" "/usr/local"; do
  case $yn in
    "$PWD" ) prearg=$(echo "-DCMAKE_INSTALL_PREFIX=$PWD"); break;;
    "/usr/local" ) prearg=$(echo "-DCMAKE_INSTALL_PREFIX=/usr/local"); break;;
  esac
done
echo "Test unsupported?"
select yn in "Do-test" "Do-not"; do
  case $yn in
    Do-test ) unsup=$(echo "EIGEN_LEAVE_TEST_IN_ALL_TARGET=FALSE"); break;;
    Do-not ) unsup=$(echo ""); break;;
  esac
done
cmake ~rep/eigen $prearg $unsup
echo "make check?"
select yn in "Do-check" "Do-not"; do
  case $yn in
    Do-check ) make check -j4; break;;
    Do-not ) kill $$; break;;
  esac
done


