#!/bin/zsh

while inotifywait -e modify -r ~rep/eigen;
 do
   cd ~bld/eigen && make install -j3 && make check -j3 > ~bld/eigen/testout
  cat testout | grep '#' | xmessage -file -
 done
  
