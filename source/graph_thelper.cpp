#include <iostream>
#include <fstream>
using namespace std;
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

void add_lines( ofstream* out, ifstream* in, int chop ){
  string line;
  for( int i = 0; i < 6; ++i ){
    getline (*in,line);
    line = line.substr( chop );
    *out << line << "\n";
    getline (*in,line);
    getline (*in,line);
    getline (*in,line);
  }
  getline (*in,line);
  line = line.substr( chop );
  *out << line << "\n";
}

void example_lines( ofstream* out, ifstream* in){
  string line;
  for( int i = 0; i < 6; ++i ){
    getline (*in,line);
    line = line.substr( 40 );
    *out << line << "\n";
    getline (*in,line);
    getline (*in,line);
  }
  getline (*in,line);
  line = line.substr( 40 );
  *out << line << "\n";
}

int main () {
  string line;
  ofstream myfileo;
  myfileo.open ("easy_timings.txt", ios::out);
  std::vector<bool> goodLines(3000,false);
  vector<string> testStrs;
  int myints[] = {16, 15, 27,23,39,34, 23};
  vector<int> offset( myints, myints + sizeof(myints) / sizeof(int) );
  string cpStr = "cp mix  test - 2097152";
  testStrs.push_back( cpStr );
  string hStr = "h test - 2097152";
  testStrs.push_back( hStr );
  string rStr = "Reaction rates test - 2097152";
  testStrs.push_back( rStr );
  string tStr = "T from E0 test - 2097152";
  testStrs.push_back( tStr );
  string dStr = "Diffusion Coefficient Mol test - 2097152";
  testStrs.push_back( dStr );
  string tcStr = "Thermal Conductivity test - 2097152";
  testStrs.push_back( tcStr );
  string vStr = "Viscosity test - 2097152";
  testStrs.push_back( vStr );
  string exStr = "PoKiTT Reaction Diffusion size 524288";
  int i=1;
  int n=0;
  bool info = false;
  ifstream myfile ("timings.txt");
  if (myfile.is_open())
  {
    getline (myfile,line); ++i;
    myfileo << line << "\n";
    getline (myfile,line); ++i;
    myfileo << line << "\n";
    while ( getline (myfile,line) )
    {
      if( !info ){
        if( line.compare( "BUILD INFORMATION:" ) == 0 ){
          myfileo << line << "\n";
          getline (myfile,line); ++i;
          getline (myfile,line); ++i;
          myfileo << line << "\n";
          getline (myfile,line); ++i;
          myfileo << line << "\n";
          getline (myfile,line); ++i;
          myfileo << line << "\n";
          info = true;
        }
      }
      for( vector<string>::iterator it = testStrs.begin(); it != testStrs.end(); ++it){
        if( line.compare( *it ) == 0 ){
          myfileo << line << "\n";
          add_lines(&myfileo, &myfile, offset[n]);
          ++n;
          i = i + 25;
        }
      }
      if( line.compare( exStr ) == 0 ){
        myfileo << line << "\n";
        example_lines(&myfileo, &myfile);
        i = i + 18;
      }
      ++i;
    }
    myfile.close();
  }


  myfileo.close();
  return 0;
}
