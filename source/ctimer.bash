#!/bin/bash

#USAGE:
# This needs to be run in the directory one level above the build directory
# ./timer.bash $build_directory $xml_file $num_pokitt_reps
# e.g. if I have a directory ~/pokitt/gpu-build and I want to time methanol.xml averaged 10 times
# I would put timer.bash in directory pokitt and run using
# ./timer.bash gpu-build methanol.xml 10
# the output is piped to gpu-build/test/timings.txt
# Lines 71-75 run an executable which cuts out everything except the specific timings used in the Google sheet
# To compile thelper, copy timing_helper.cpp to the same directory as timer.bash and compile using
# g++ timing_help.cpp -o thelper
# thelper is almost guaranteed to break if the numbering of lines is changed in any way
# However, you can edit timing_helper.cpp and adjust it to the new format
# When using thelper, the trimmed file is renamed easy_timings$xml_file.txt in the same directory as timings.txt

date;
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
root=$DIR/$1
path=$root/test;
rm $path/timings.txt;
touch $path/timings.txt;
date > $path/timings.txt;
echo Number of $1 $2 repeats=$3;
echo Number of $1 $2 repeats=$3 >> $path/timings.txt;

echo export CANTERA_DATA=$path;
export CANTERA_DATA=$path;

echo $path/thermo_test --xml-input-file=$2 --mix --timings --cantera-reps=$3 --disable-cv --disable-h --disbale-e
echo $path/thermo_test --xml-input-file=$2 --mix --timings --cantera-reps=$3 --disable-cv --disable-h --disbale-e >> $path/timings.txt;
$path/thermo_test --xml-input-file=$2 --mix --timings --cantera-reps=$3  --disable-cv --disable-h --disable-e >> $path/timings.txt;

echo $path/thermo_test --xml-input-file=$2 --timings --cantera-reps=$3 --disable-cv --disable-cp --disable-e
echo $path/thermo_test --xml-input-file=$2 --timings --cantera-reps=$3 --disable-cv --disable-cp --disable-e >> $path/timings.txt;
$path/thermo_test --xml-input-file=$2 --timings --cantera-reps=$3  --disable-cv --disable-cp --disable-e >> $path/timings.txt;

echo $path/rxn_test --xml-input-file=$2 --timings --cantera-reps=$3
echo $path/rxn_test --xml-input-file=$2 --timings --cantera-reps=$3 >> $path/timings.txt;
$path/rxn_test --xml-input-file=$2 --timings --cantera-reps=$3 >> $path/timings.txt;

echo $path/temperature_test --xml-input-file=$2 --timings --cantera-reps=$3 --disable-t-from-h
echo $path/temperature_test --xml-input-file=$2 --timings --cantera-reps=$3 --disable-t-from-h >> $path/timings.txt;
$path/temperature_test --xml-input-file=$2 --timings --cantera-reps=$3 --disable-t-from-h >> $path/timings.txt;

echo $path/transport_test --xml-input-file=$2 --timings --cantera-reps=$3--disable-d-mass
echo $path/transport_test --xml-input-file=$2 --timings --cantera-reps=$3 --disable-d-mass >> $path/timings.txt;
$path/transport_test --xml-input-file=$2 --timings --cantera-reps=$3 --disable-d-mass >> $path/timings.txt;

examples=$root/examples/ReactionDiffusion;
echo $examples/rxndiff --xml-input-file=$2 --timings --nsteps=1
echo $examples/rxndiff --xml-input-file=$2 --timings --nsteps=1 >> $path/timings.txt;
$examples/rxndiff --xml-input-file=$2 --timings --nsteps=1 >> $path/timings.txt;

echo finished;

echo cd $path;
cd $path;

echo ../../cthelper;
../../cthelper;

echo cp $path/easy_timings.txt $path/easy_ctimings$2.txt;
cp $path/easy_timings.txt $path/easy_ctimings$2.txt;

echo finished;
