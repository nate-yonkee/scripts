#!/bin/bash

grey='\e[01;97;40m'
no_color='\e[0m'
if [[ $LOGNAME = "nate" ]];
then
echo -e "${grey}$1${no_color}"
else
gecho -e "${grey}$1${no_color}"
fi
