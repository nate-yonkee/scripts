#!/bin/bash
hstep=400
vstep=480
vsize=1200
hsize=1920
thick=2
let vresSize=vstep-thick
rad=4
xone=132
xtwo=180
xthr=144
ystart=147
let yone=ystart
let ytwo=227
let ythr=160

LPOne=240
let RPOne=LPOne-Scnd+250
let RRPOne=RPOne-Scnd+200
let off=1199-vsize
let RPTwo=hsize-RPOne
#let LPTwo=hsize-RPOne
vinit="mousemove $vstep 0"
hinit="mousemove 0 $hstep"

vdraw="mousermove $thick $vsize"
hdraw="mousermove $hsize $thick"

vreset="mousermove $vresSize -$vsize"
hreset="mousermove -$hsize $hstep"

dotone="mousemove $xone $ystart"
dottwo="mousermove 0 $yone"
dotthr="mousermove 0 $ytwo"
dotfour="mousermove 0 $ythr"
btwid="mousermove -$rad -$rad"
let rad=rad+rad
twid="mousermove $rad $rad"

xte "$(echo $vinit)" 'mousedown 8' 'usleep 10000' "$(echo $vdraw)" 'mouseup 8' 'usleep 10000'
xte "$(echo $vreset)" 'mousedown 8' 'usleep 10000' "$(echo $vdraw)" 'mouseup 8' 'usleep 10000'
xte "$(echo $vreset)" 'mousedown 8' 'usleep 10000' "$(echo $vdraw)" 'mouseup 8' 'usleep 10000'

xte "$(echo $hinit)" 'mousedown 8' 'usleep 10000' "$(echo $hdraw)" 'mouseup 8' 'usleep 10000'
xte "$(echo $hreset)" 'mousedown 8' 'usleep 10000' "$(echo $hdraw)" 'mouseup 8' 'usleep 10000'

# 1
xte "$(echo $dotone)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotfour)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad

# 2
let newx=$xone+$xone
dotone="mousemove $newx $ystart"
xte "$(echo $dotone)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotfour)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad

# 3
let newx=$newx+$xone
dotone="mousemove $newx $ystart"
xte "$(echo $dotone)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotfour)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad

#echo 4
let newx=$newx+$xtwo
dotone="mousemove $newx $ystart"
xte "$(echo $dotone)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotfour)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad

#echo 5
let newx=$newx+$xthr
dotone="mousemove $newx $ystart"
xte "$(echo $dotone)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotfour)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad

#echo 6
let newx=$newx+$xthr
dotone="mousemove $newx $ystart"
xte "$(echo $dotone)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotfour)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad

#echo 7
let newx=$newx+96+96
dotone="mousemove $newx $ystart"
xte "$(echo $dotone)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotfour)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad

let newx=$newx+$xthr
dotone="mousemove $newx $ystart"
xte "$(echo $dotone)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotfour)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad

let newx=$newx+$xthr
dotone="mousemove $newx $ystart"
xte "$(echo $dotone)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotfour)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad

let newx=$newx+$xtwo
dotone="mousemove $newx $ystart"
xte "$(echo $dotone)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotfour)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad

let newx=$newx+$xone
dotone="mousemove $newx $ystart"
xte "$(echo $dotone)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotfour)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad

let newx=$newx+$xone
dotone="mousemove $newx $ystart"
xte "$(echo $dotone)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotfour)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dotthr)"
/home/nate/scripts/plus.bash $rad
xte "$(echo $dottwo)"
/home/nate/scripts/plus.bash $rad

nOneLinit="mousemove $LPOne $vsize"
nOneRinit="mousemove $RPOne $vsize"
nOneRRinit="mousemove $RRPOne $vsize"

#nTwoLinit="mousemove $LPTwo $vsize"
nTwoRinit="mousemove $RPTwo $vsize"

nBothEnd="mousermove 0 $off"
nBothScnd="mousermove $Scnd -$off"

#xte "$(echo $nOneLinit)"  'mousedown 9' 'usleep 100' "$(echo $nBothEnd)" 'mouseup 9' 'usleep 100000'
#xte "$(echo $nBothScnd)"  'mousedown 9' 'usleep 100' "$(echo $nBothEnd)" 'mouseup 9' 'usleep 100000'

#xte "$(echo $nOneRinit)"  'mousedown 9' 'usleep 100' "$(echo $nBothEnd)" 'mouseup 9' 'usleep 100000'
#xte "$(echo $nBothScnd)"  'mousedown 9' 'usleep 100' "$(echo $nBothEnd)" 'mouseup 9' 'usleep 100000'

#xte "$(echo $nOneRRinit)" 'mousedown 9' 'usleep 100' "$(echo $nBothEnd)" 'mouseup 9' 'usleep 100000'
#xte "$(echo $nBothScnd)"  'mousedown 9' 'usleep 100' "$(echo $nBothEnd)" 'mouseup 9' 'usleep 100000'

#xte "$(echo $nTwoLinit)" 'mousedown 9' 'usleep 100' "$(echo $nBothEnd)" 'mouseup 9' 'usleep 100000'
#xte "$(echo $nBothScnd)" 'mousedown 9' 'usleep 100' "$(echo $nBothEnd)" 'mouseup 9' 'usleep 100000'

#xte "$(echo $nTwoRinit)" 'mousedown 9' 'usleep 100' "$(echo $nBothEnd)" 'mouseup 9' 'usleep 100000'
#xte "$(echo $nBothScnd)" 'mousedown 9' 'usleep 100' "$(echo $nBothEnd)" 'mouseup 9' 'usleep 100000'
