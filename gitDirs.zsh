#!/bin/zsh
startdir=$PWD
target="$PWD/automated_testr/$(date '+%d-%b-%Y_%H-%M')"
echo "where to do the testing [$target]"
read workdir

if [ -z "$workdir" ]; then
  if [ ! -d "$target" ]; then
    mkdir -p "$target"
  fi
  base=$target
fi
if [ -n "$workdir" ]; then
  base=$workdir
fi

cd $base
ls -a
echo "I am going to erase everything in $base - enter any text then Return to exit - to continue, press Return alone"
read any
if [ -n "$any" ]; then
  exit
fi
ls -a
echo "are you really sure? enter any text then Return to exit - to continue, press Return alone"
read any1
if [ -n "$any1" ]; then
  exit
fi
rm -rf $base/*
gld=$base/gld/
bld=$base/bld/
mkdir $gld
mkdir $bld

cd $gld
git clone https://software.crsim.utah.edu:8443/James_Research_Group/SpatialOps.git
git clone https://software.crsim.utah.edu:8443/James_Research_Group/ExprLib.git
git clone https://software.crsim.utah.edu:8443/James_Research_Group/ODT.git
git clone https://software.crsim.utah.edu:8443/James_Research_Group/Cantera.git
git clone https://software.crsim.utah.edu:8443/James_Research_Group/PoKiTT.git
git clone https://software.crsim.utah.edu:8443/James_Research_Group/NSCBC.git
git clone https://software.crsim.utah.edu:8443/James_Research_Group/LBMS.git
gso=$gld/SpatialOps
gex=$gld/ExprLib
god=$gld/ODT
gca=$gld/Cantera
gpo=$gld/PoKiTT
gns=$gld/NSCBC
glb=$gld/LBMS
cd "$gso" && git checkout -B master
cd "$gex" && git checkout -B master
cd "$god" && git checkout -B master
cd "$gca" && git checkout -B master
cd "$gpo" && git checkout -B master
cd "$gns" && git checkout -B master
cd "$glb" && git checkout -B master

sbld=$bld/s-spt
nowgld=$gso
sopt="-DCMAKE_BUILD_TYPE=Release"
mkdir -p $sbld && cd $sbld \
&& cmake "$nowgld" "$sopt" \
&& make install -j4 && make test

mbld=$bld/m-spt
mopt="-DCMAKE_BUILD_TYPE=Release -DENABLE_THREADS=ON -DNTHREADS=3"
mkdir -p $mbld && cd $mbld \
&& cmake "$nowgld" "$mopt" \
&& make install -j4 && make test

echo "test SpatialOps now? to not test send empty return - to test send anything"
read wtest
if [ -n "$wtest" ]; then
  echo "Where is your working dir for SpatialOps? [ $SOWORKDIR ]"
  read loc
  if [ -z "$loc" ]; then
    workdir=$SOWORKDIR
  fi
  if [ -n "$loc" ]; then
    workdir=$loc
  fi

  nowgold=$workdir
  rm -rf $sbld && mkdir -p $sbld && cd $sbld \
  && cmake $nowgld $sopt \
  && make install -j4 && make test

  rm -rf $mbld && mkdir -p $mbld && cd $mbld \
  && cmake $nowgld $mopt \
  && make install -j4 && make test
fi

cd "$startdir"
