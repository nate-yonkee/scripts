#!/bin/zsh

while true
do
  dmenu -fn "-xos4-terminus-medium-r-*-*-24-*" -l 25 < ~/.xmonad/ctlfifo  | cut -c $1- | xargs ~nate/.xmonad/ctlxmo
done
