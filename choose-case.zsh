#!/bin/zsh

echo "Do you wish to say yes or no? (enter #)"
select yn in "Yes" "No"; do
  case $yn in
    Yes ) echo "yes"; break;;
    No ) vared -p 'say something ' -c tmp;  break;;
  esac
done
if [ -n "$tmp" ]; then
echo $tmp
fi