#!/bin/zsh
xtrlock &
plock=$!
echo "you are locked!" | xmessage -file - -center&
pid1=$!
xclock -d -update 1 -padding 800&
pid2=$!
wait $plock
kill $pid2
kill $pid1
